import Routing
import FluentSQLite
import Vapor

/// Called after your application has initialized.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#bootswift)
public func boot(_ app: Application) throws {
    let _ = app.withConnection(to: .sqlite) { db -> Future<Kind> in
        Kind.query(on: db).all().flatMap(to: Kind.self, { (arrayKinds) -> EventLoopFuture<Kind> in
            if !arrayKinds.isEmpty {
                throw Abort(.preconditionFailed)
            }
            let _ = Kind(description: "Amigo").save(on: db)
            let _ = Kind(description: "Comercial").save(on: db)
            return Kind(description: "Conhecido").save(on: db)
        })
    }
}
