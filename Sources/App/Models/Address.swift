//
//  Adress.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor
import FluentSQLite

final class Address: SQLiteModel {
    var id: Int?
    
    var street: String
    var city: String
    var contactID: Contact.ID
    
    var contact: Parent<Address, Contact> {
        return parent(\.contactID)
    }
    
    init(street: String, city: String, contactID: Contact.ID) {
        self.street = street
        self.city = city
        self.contactID = contactID
    }
}

extension Address: Migration {}

extension Address: Content {}

extension Address: Parameter {}
