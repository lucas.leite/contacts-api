//
//  Kind.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor
import FluentSQLite

final class Kind: SQLiteModel {
    var id: Int?
    var description: String
    
    init(description: String) {
        self.description = description
    }
}

extension Kind: Migration {}

extension Kind: Content {}
