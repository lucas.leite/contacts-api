//
//  Contact.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor
import FluentSQLite

final class Contact: SQLiteModel {
    var id: Int?
    
    var name: String
    var email: String
    var birthdate: Date
    var kindID: Kind.ID
    
    var kind: Parent<Contact, Kind> {
        return parent(\.kindID)
    }
    
    var phones: Children<Contact, Phone> {
        return children(\.contactID)
    }
    
    var addresses: Children<Contact, Address> {
        return children(\.contactID)
    }
    
    init(name: String, email: String, birthdate: Date, kindID: Kind.ID) {
        self.name = name
        self.email = email
        self.birthdate = birthdate
        self.kindID = kindID
    }
}

extension Contact: Migration {}

extension Contact: Content {}

extension Contact: Parameter {}
