//
//  Phone.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor
import FluentSQLite

final class Phone: SQLiteModel {
    var id: Int?
    var number: String
    var contactID: Contact.ID?
    
    var contact: Parent<Phone, Contact> {
        return parent(\.contactID)!
    }
    
    init(number: String) {
        self.number = number
    }
}

extension Phone: Migration {}

extension Phone: Content {}

extension Phone: Parameter {}
