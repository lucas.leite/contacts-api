import Routing
import Vapor

/// Register your application's routes here.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#routesswift)
public func routes(_ router: Router) throws {
    let contactsController = ContactsController()
    try router.register(collection: contactsController)
    
    let addressesController = AddressesController()
    try router.register(collection: addressesController)
    
    let phonesController = PhonesController()
    try router.register(collection: phonesController)
    
    let kindsController = KindsController()
    try router.register(collection: kindsController)
}
