//
//  ContactsController.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor

struct ContactsController: RouteCollection {
    func boot(router: Router) throws {
        let groupedRoutes = router.grouped("contacts")
        groupedRoutes.get(use: getAllContactsHandler)
        groupedRoutes.get(Contact.parameter, use: getContactHandler)
        groupedRoutes.post(use: createContactHandler)
        groupedRoutes.delete(Contact.parameter, use: deleteContactHandler)
        groupedRoutes.put(Contact.parameter, use: updateContactHandler)
    }
    
    func getAllContactsHandler(_ req: Request) -> Future<[Contact]> {
        return Contact.query(on: req).all()
    }
    
    func getContactHandler(_ req: Request) throws -> Future<Contact> {
        return try req.parameter(Contact.self)
    }
    
    func createContactHandler(_ req: Request) throws -> Future<Contact> {
        return try req.content.decode(Contact.self).save(on: req)
    }
    
    func deleteContactHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameter(Contact.self).delete(on: req).transform(to: .noContent)
    }
    
    func updateContactHandler(_ req: Request) throws -> Future<Contact> {
        return try flatMap(to: Contact.self, req.parameter(Contact.self), req.content.decode(Contact.self)) { oldContact, newContact -> Future<Contact> in
            oldContact.name = newContact.name
            oldContact.email = newContact.email
            oldContact.birthdate = newContact.birthdate
            
            return oldContact.save(on: req)
        }
    }
}
