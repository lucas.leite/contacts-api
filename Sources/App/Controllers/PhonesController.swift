//
//  PhonesController.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor

struct PhonesController: RouteCollection {
    func boot(router: Router) throws {
        router.post("phones", use: createPhoneHandler)
        
        let groupedRoutes = router.grouped("phones", Phone.parameter)
        groupedRoutes.put(use: updatePhoneHandler)
        groupedRoutes.delete(use: deletePhoneHandler)
        
        let contactsGroupedRoutes = router.grouped("contacts", Contact.parameter, "phones")
        contactsGroupedRoutes.get(use: getContactPhonesHandler)
    }
    
    func createPhoneHandler(_ req: Request) throws -> Future<Phone> {
        return try req.content.decode(Phone.self).flatMap(to: Phone.self) { phone -> Future<Phone> in
            return try phone.contact.get(on: req).flatMap(to: Phone.self) { contact -> Future<Phone> in
                return phone.save(on: req)
            }
        }
    }
    
    func getContactPhonesHandler(_ req: Request) throws -> Future<[Phone]> {
        return try req.parameter(Contact.self).flatMap(to: [Phone].self) { contact -> Future<[Phone]> in
            return try contact.phones.query(on: req).all()
        }
    }
    
    func updatePhoneHandler(_ req: Request) throws -> Future<Phone> {
        return try flatMap(to: Phone.self, req.parameter(Phone.self), req.content.decode(Phone.self)) { oldPhone, newPhone -> Future<Phone> in
            oldPhone.number = newPhone.number
            
            return oldPhone.save(on: req)
        }
    }
    
    func deletePhoneHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameter(Phone.self).delete(on: req).transform(to: HTTPStatus.noContent)
    }
}
