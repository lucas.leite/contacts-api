//
//  KindsController.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor

struct KindsController: RouteCollection {
    func boot(router: Router) throws {
        let groupedRouterKinds = router.grouped("kinds")
        groupedRouterKinds.post(use: createKindHandler)
        groupedRouterKinds.get(use: getAllKindsHandler)
        
        router.get("contacts", Contact.parameter, "kind", use: getContactKindHandler)
    }
    
    func createKindHandler(_ req: Request) throws -> Future<Kind> {
        return try req.content.decode(Kind.self).save(on: req)
    }
    
    func getAllKindsHandler(_ req: Request) -> Future<[Kind]> {
        return Kind.query(on: req).all()
    }
    
    func getContactKindHandler(_ req: Request) throws -> Future<Kind> {
        return try req.parameter(Contact.self).flatMap(to: Kind.self) { contact -> Future<Kind> in
            return try contact.kind.get(on: req)
        }
    }
}
