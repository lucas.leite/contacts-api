//
//  AddressesController.swift
//  App
//
//  Created by Lucas dos Santos on 13/04/2018.
//

import Vapor

struct AddressesController: RouteCollection {
    func boot(router: Router) throws {
        router.get("contacts", Contact.parameter, "addresses", use: getContactAddressesHandler)
        let groupedRoutes = router.grouped("addresses")
        groupedRoutes.post(use: createAddressHandler)
        groupedRoutes.get(Address.parameter, use: getAddressHandler)
    }
    
    func getContactAddressesHandler(_ req: Request) throws -> Future<[Address]> {
        return try req.parameter(Contact.self).flatMap(to: [Address].self) { contact -> Future<[Address]> in
            return try contact.addresses.query(on: req).all()
        }
    }
    
    func createAddressHandler(_ req: Request) throws -> Future<Address> {
        return try req.content.decode(Address.self).save(on: req)
    }
    
    func getAddressHandler(_ req: Request) throws -> Future<Address> {
        return try req.parameter(Address.self)
    }
}
